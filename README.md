# Pre-requisites
* A Linux based distribution for running the PoC
* [Git client (cli)](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [Docker](https://docs.docker.com/get-docker/)
* [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* [KinD](https://kind.sigs.k8s.io/docs/user/quick-start/)
* [Helm3](https://helm.sh/docs/intro/install/)

# Getting the project
Open a terminal and create a working directory for your PoC, like:
```
mkdir liquibase-poc
cd liquibase-poc
git clone $REPOURL environment
```
It will bring the environment for the PoC in the "environment" directory. Note $REPOURL is the git URL with the PoC environment project.

Additionaly you can get another git project to test the migrations or create it from scratch, but the idea is to have in the same level of liquibase-poc with the "environment" directory the the following file path: ```db-model-example/changelogs/changelog.sql```, so you end with the following path structure like:
```
iquibase-poc
|-- db-model-example
|   `-- changelogs
|       `-- changelog.sql
`-- environment
    |-- .gitignore
    |-- KinD
    |   |-- config.yml
    |   `-- create_kind_cluster.sh
    |-- README.md
    |-- helm
    |   `-- liquibase-poc
    |       |-- .helmignore
    |       |-- Chart.lock
    |       |-- Chart.yaml
    |       |-- charts
    |       |   `-- postgresql-8.9.8.tgz
    |       `-- values.yaml
    `-- liquibase
        |-- expose_pgsql.sh
        `-- run.sh
```

# Starting the environment
1. Move to environment: ```cd environment/```
2. Run KinD: ```KinD/create_kind_cluster.sh```
3. Ensure KinD was correctly created and kubectl is pointing to it (127.0.0.1): ```kubectl cluster-info```
4. Install the environment: ```helm install liquibase helm/liquibase-poc```
5. Wait for services to start and get "Ready", you can check them with: ```kubectl get pods```
6. Expose the services (like postgres) ports to your host: ```liquibase/expose_*.sh```

You should be able to connect to your new created postgres instance with "admin" password: ```PGPASSWORD=admin psql --host 127.0.0.1 -U postgres -d postgres -p 5432```

# Test Liquibase
At this point, I assume you should have a changelog.sql file. Now you can run migrations with the run.sh script which uses a dockerized liquibase so you don't need to download the binary. A little guide:
* Create a DB tag: ```liquibase/run.sh tag 1.2.1```
* Render update without applying it: ```liquibase/run.sh updateSQL```
* Apply update: ```liquibase/run.sh update```
* Render rollback without applying it: ```liquibase/run.sh rollbackSQL <tag>```
* Apply rollback: ```liquibase/run.sh rollback <tag>```

To learn more about liquibase, refer to: [https://www.liquibase.org/get-started](https://www.liquibase.org/get-started)
