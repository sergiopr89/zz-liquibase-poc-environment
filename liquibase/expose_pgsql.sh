#!/bin/bash

kubectl port-forward --namespace default svc/liquibase-postgresql 5432:5432 >/dev/null &
