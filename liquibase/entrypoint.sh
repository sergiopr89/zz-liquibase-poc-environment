#!/bin/bash

grep -E "^pgsanity.*" <(echo $1) &>/dev/null
if [[ $? -eq 0 ]]; then
  $@
else
  /liquibase/liquibase $@
fi
