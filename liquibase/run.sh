#!/bin/bash

IMAGE_NAME="liquibase"
CHANGELOG_PATH=$(readlink -f $(dirname $(readlink -f $0))/../../db-model-example/changelogs)
if [[ $1 == "pgsanity" ]]; then
  docker run --network host --rm -v ${CHANGELOG_PATH}:/liquibase/changelog ${IMAGE_NAME} $@
else
  docker run --network host --rm -v ${CHANGELOG_PATH}:/liquibase/changelog ${IMAGE_NAME} --url="jdbc:postgresql://127.0.0.1:5432/postgres?currentSchema=public" --changeLogFile=/liquibase/changelog/main.xml --username=postgres --password=admin $@
fi
