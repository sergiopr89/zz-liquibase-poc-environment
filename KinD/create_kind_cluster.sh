#!/bin/bash

BASEDIR=$(dirname $(readlink -f $0))
KLUSTER_NAME="liquibase-poc"

# Delete old cluster if exists
kind delete cluster --name ${KLUSTER_NAME}

# Create a new one
kind create cluster --config ${BASEDIR}/config.yml --name ${KLUSTER_NAME}
